.. wmi nipype workflows documentation master file, created by
   sphinx-quickstart on Tue Nov 24 10:25:51 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to wmi nipype workflows's documentation!
================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   register_to_mni

.. autofunction:: wmi_nipype_workflows.




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
