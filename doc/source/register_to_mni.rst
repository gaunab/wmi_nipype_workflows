.. workflow ::
    :graph2use: flat
    :simple_form: no

    from niflow.nipype1.workflows.dmri.camino.connectivity_mapping import create_connectivity_pipeline
    wf = create_connectivity_pipeline()
