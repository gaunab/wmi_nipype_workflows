wmi\_nipype\_workflows package
==============================

Submodules
----------

wmi\_nipype\_workflows..dmri\_workflow.py\@neomake\_26349\_18 module
--------------------------------------------------------------------

.. automodule:: wmi_nipype_workflows..dmri_workflow.py@neomake_26349_18
   :members:
   :undoc-members:
   :show-inheritance:

wmi\_nipype\_workflows.brainextract\_workflow module
----------------------------------------------------

.. automodule:: wmi_nipype_workflows.brainextract_workflow
   :members:
   :undoc-members:
   :show-inheritance:

wmi\_nipype\_workflows.despot\_workflow module
----------------------------------------------

.. automodule:: wmi_nipype_workflows.despot_workflow
   :members:
   :undoc-members:
   :show-inheritance:

wmi\_nipype\_workflows.dmri\_workflow module
--------------------------------------------

.. automodule:: wmi_nipype_workflows.dmri_workflow
   :members:
   :undoc-members:
   :show-inheritance:

wmi\_nipype\_workflows.image\_statistik module
----------------------------------------------

.. automodule:: wmi_nipype_workflows.image_statistik
   :members:
   :undoc-members:
   :show-inheritance:

wmi\_nipype\_workflows.lst module
---------------------------------

.. automodule:: wmi_nipype_workflows.lst
   :members:
   :undoc-members:
   :show-inheritance:

wmi\_nipype\_workflows.masking module
-------------------------------------

.. automodule:: wmi_nipype_workflows.masking
   :members:
   :undoc-members:
   :show-inheritance:

wmi\_nipype\_workflows.preproc module
-------------------------------------

.. automodule:: wmi_nipype_workflows.preproc
   :members:
   :undoc-members:
   :show-inheritance:

wmi\_nipype\_workflows.register\_session\_wf module
---------------------------------------------------

.. automodule:: wmi_nipype_workflows.register_session_wf
   :members:
   :undoc-members:
   :show-inheritance:

wmi\_nipype\_workflows.register\_to\_mni module
-----------------------------------------------

.. automodule:: wmi_nipype_workflows.register_to_mni
   :members:
   :undoc-members:
   :show-inheritance:

wmi\_nipype\_workflows.reports module
-------------------------------------

.. automodule:: wmi_nipype_workflows.reports
   :members:
   :undoc-members:
   :show-inheritance:

wmi\_nipype\_workflows.utils module
-----------------------------------

.. automodule:: wmi_nipype_workflows.utils
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: wmi_nipype_workflows
   :members:
   :undoc-members:
   :show-inheritance:
