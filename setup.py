#! env python

# -*- coding: utf-8 -*-

import os
from distutils.core import setup

setup(
    name = "wmi nipype workflows",
    version = "0.0.7",
    license='MIT',
    classifiers=[
        'Programming Language :: Python :: 3',
        ],
    description = 'Nipype Workflows for use in WMI Research Group',
    author = 'Paul kuntke',
    author_email = 'paul.kuntke@ukdd.de',
    packages = ['wmi_nipype_workflows'],
    install_requires=["nipype", "nilearn",  "matplotlib"],
    package_data={
        'wmi_nipype_workflows': ["assets/index.html", "assets/_assets/*/*"]
        }
)
