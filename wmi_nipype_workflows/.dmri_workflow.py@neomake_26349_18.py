#! env python
# -*- coding: utf-8 -*-

__author__ = "Paul Kuntke"
__email__ = "paul.kuntke@ukdd.de"
__license__ = "BSD-3-Clause"

from nipype.workflows.dmri.fsl.utils import b0_average

def dmri_brainmask():
    """ Generate a workflow to extract the brainmask from dti-image. By using the bval-File
        all B0 values are used to generate a mean B0-File. From this the brain will be extracted.


        Inputs::
            - inputnode.in_dwi: the actual dmri-image file (nii.gz)
            - inputnode.in_bval: the bval-file for the given image

        Outputs::
            - outputnode.brainmask: the mask 

    """


    inputnode = Node(IdentityInterface(fields=['in_dmri', 'in_bval']), name='inputnode')
    avg_b0 = Node(Function(input_names=['in_dwi', 'in_bval'],
                           output_names=['out_file'],
                           function=b0_average),
                  name='b0_avg')
    brainmask = Node(BET(robust=True, mask=True), name='brainmask')
    outputnode = Node(IdentityInterface(fields=['brainmask']), name='outputnode')

    wf = Workflow(name="dti_brainmask")
    wf.connect([(inputnode, avg_b0, [('in_dmri', 'in_dwi'),
                                     ('in_bval', 'in_bval')]),
                (avg_b0, brainmask, [('out_file', 'in_file')]),
                (brainmask, outputnode, [('mask_file', 'brainmask')])
               ])
    return wf

def dmri_preproc():
    """ Prepropcess dmri-files and extract FA, AD, RD

    """

    def _gen_index(in_file):
        """ Generate the index-file from given dmri_nii.gz for use with eddy """
        from os.path import isfile
        import numpy as np
        import nibabel as nb
        from os.path import abspath
        out_file = abspath('index.txt')
        volcount = nb.load(in_file).get_data().shape[-1]
        index = np.ones((volcount,)).T

        # The index-file is only rewritten if it differs from orignal one
        if isfile(out_file):
            old_index = np.loadtxt(out_file)
            if not (old_index.all() ==  index.all() and old_index.shape == index.shape):
                np.savetxt(out_file, index)
        else:
            np.savetxt(out_file, index)
        return out_file

    def _gen_acqp():
        """" generate acqp textfie """
        import numpy as np
        from os.path import isfile
        from os.path import abspath
        echo = 0.95 # echo-spacing
        epi = 96 # epi-factor
        
        

        acqp = [0, -1, 0, 0.001 * echo * epi] # 0,-1 => Anterior->Posterior
        out_file = abspath('acqp.txt')
        # acqp-file is only rewritten if it differs from original one
        if isfile(out_file):
            old_acqp = np.loadtxt(out_file, delimiter=' ')
            if not(old_acqp.all() == np.array(acqp).all() and old_acqp.shape == np.array(acqp).shape):
                np.savetxt(out_file, np.atleast_2d(acqp), fmt="%s", delimiter=' ')
        return out_file
    def _calculate_MD(l1, l2, l3):
        """ Calculate MD-File """
        import nibabel as nib
        from os.path import abspath, basename

        l1_data = nib.load(l1).get_data()
        l2_data = nib.load(l2).get_data()
        l3_data = nib.load(l3).get_data()

        md_data = (l1_data + l2_data + l3_data) / 3.0

        orig = nib.load(l1)
        fname = basename(l1)
        out_file = abspath(fname.replace("lambda1", "MD"))
        nib.save(nib.Nifti1Image(md_data, orig.affine, orig.header), out_file)

        return out_file

    def _calculate_RD(l2,l3):
        """ Calculate RD-Files

        Parameter
        ----------
        l2,l3: str
            filenames of lambda_files 

        Return
        ------
        filename: str

        """
        from os.path import abspath, basename
        import nibabel as nib

        l2_data = nib.load(l2).get_data()
        l3_data = nib.load(l3).get_data()
        rd_data = (l2_data + l3_data) / 2.0
        orig = nib.load(l2)
        fname = basename(l2)
        out_file = abspath(fname.replace("lambda2", "RD"))
        nib.save(nib.Nifti1Image(rd_data, orig.affine, orig.header), out_file)

        return out_file

    inputnode = Node(IdentityInterface(fields=['in_bval', 'in_bvec', 'in_dmri']), name='inputnode')
    mask = dmri_brainmask()
    eddy = Node(Eddy(num_threads=16), name='eddy')
    eddy.inputs.in_acqp = _gen_acqp()
    eddy.inputs.num_threads = 1
    eddyindex = Node(Function(function=_gen_index, input_names=["in_file"],
                              output_names=["index"]), name="eddyindex")

    dtifit = Node(DTIFit(), name='dtifit')
    rd_image = Node(Function(function=_calculate_RD, input_names=["l2","l3"],
                         output_names=["out_file"]), name="rd_image")
    outputnode = Node(IdentityInterface(fields=['fa', 'md', 'l1', 'l2', 'l3', 'rd', 'ad', 'mask']), name='outputnode')

    wf = Workflow(name='dmri_preproc')
    wf.connect([(inputnode, mask, [('in_bval', 'inputnode.in_bval'),
                                   ('in_dmri', 'inputnode.in_dmri')]),
                (inputnode, eddyindex, [('in_dmri', 'in_file')]),
                (inputnode, eddy, [('in_dmri', 'in_file'),
                                   ('in_bvec', 'in_bvec'),
                                   ('in_bval', 'in_bval')]),
                (eddyindex, eddy, [('index', 'in_index')]),
                (inputnode, dtifit, [('in_bval', 'bvals'),
                                     ('in_bvec', 'bvecs')]),
                (mask, eddy, [('outputnode.brainmask', 'in_mask')]),
                (mask, dtifit, [('outputnode.brainmask', 'mask')]),
                (eddy, dtifit, [('out_corrected','dwi')]),
                (dtifit, rd_image, [('L2', 'l2'),
                                    ('L3', 'l3')]),
                (dtifit, outputnode, [('FA', 'fa'),
                                      ('MD', 'md'),
                                      ('L1', 'l1'),
                                      ('L1', 'ad'),
                                      ('L2', 'l2'),
                                      ('L3', 'l3')]),
                (rd_image, outputnode, [('out_file', 'rd')]),
                (mask, outputnode, [('outputnode.brainmask', 'mask')])
                                   ])
    return wf

def dmri_register():
    """ Create a Workflow to register FA, MD, AD and RD Files to MNI space.
    Registration is done by using first fsl-flirt for linear registration and
    fsl-fnirt for nonlinear registration of FA first. For nonlinear
    registration the FSL-Template **FA_2_FMIRB58_1mm** is utilized.

        connnect the _inputnode_ inputs:
            - `in_fa`
            - `in_ad`
            - `in_md`
            - `in_rd`
        with the corresponing files.

        Registered Files appear in _outputnode_ as:
            - `registered_fa`
            - `registered_ad`
            - `registered_md`
            - `registered_rd`

    Returns:
    -------
        nipye.Workflow
    """


    inputnode = Node(IdentityInterface(fields=['in_fa', 'in_md', 'in_ad', 'in_rd']), name='inputnode')
    lin_reg = Node(FLIRT(output_type='NIFTI_GZ'), name='lin_reg')
    lin_reg.inputs.reference = '/usr/share/fsl/data/standard/FMRIB58_FA_1mm.nii.gz'
    nonlin_reg = Node(FNIRT(output_type='NIFTI_GZ'), name='nonlin_reg')
    nonlin_reg.inputs.field_file = 'sub_warpingfield_warp.nii.gz'
  #  nonlin_reg.inputs.warped_file = abspath('sub_FA_warp.nii.gz')
    nonlin_reg.inputs.fieldcoeff_file = True
    nonlin_reg.inputs.config_file = 'FA_2_FMRIB58_1mm'
    nonlin_reg.inputs.ref_file = '/usr/share/fsl/data/standard/FMRIB58_FA_1mm.nii.gz'
    warp_md = Node(ApplyWarp(), name='warp_md')

    warp_ad = Node(ApplyWarp(), name='warp_ad')
    warp_rd = Node(ApplyWarp(), name='warp_rd')
    warp_md.inputs.ref_file = '/usr/share/fsl/data/standard/FMRIB58_FA_1mm.nii.gz'
    warp_ad.inputs.ref_file = '/usr/share/fsl/data/standard/FMRIB58_FA_1mm.nii.gz'
    warp_rd.inputs.ref_file = '/usr/share/fsl/data/standard/FMRIB58_FA_1mm.nii.gz'
    outputnode = Node(IdentityInterface(fields=['registered_fa', 'matrix', 'warp', 'registered_md',
                                                'registered_ad', 'registered_rd']), name='outputnode')

    wf = Workflow(name='dmri_register')
    wf.connect([(inputnode, lin_reg, [('in_fa', 'in_file')]),
                (lin_reg, nonlin_reg, [('out_file', 'in_file')]),
                (nonlin_reg, outputnode, [('warped_file', 'registered_fa'),
                                          ('field_file', 'warp')]),
                (lin_reg, outputnode, [('out_matrix_file', 'matrix')]),
                (lin_reg, warp_md, [('out_matrix_file', 'premat')]),
                (lin_reg, warp_rd, [('out_matrix_file', 'premat')]),
                (lin_reg, warp_ad, [('out_matrix_file', 'premat')]),
                (nonlin_reg, warp_md, [('fieldcoeff_file', 'field_file')]),
                (nonlin_reg, warp_rd, [('fieldcoeff_file', 'field_file')]),
                (nonlin_reg, warp_ad, [('fieldcoeff_file', 'field_file')]),
                (inputnode, warp_md, [('in_md', 'in_file')]),
                (inputnode, warp_rd, [('in_rd', 'in_file')]),
                (inputnode, warp_ad, [('in_ad', 'in_file')]),
                (warp_md, outputnode, [('out_file', 'registered_md')]),
                (warp_rd, outputnode, [('out_file', 'registered_rd')]),
                (warp_ad, outputnode, [('out_file', 'registered_ad')]),

               ])

    return wf


