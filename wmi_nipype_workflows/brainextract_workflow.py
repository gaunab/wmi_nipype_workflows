#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Paul Kuntke"
__email__ = "paul.kuntke@ukdd.de"
__license__ = "BSD-3-Clause"

from nipype import Workflow, Node, IdentityInterface
from nipype.interfaces.fsl import BET, RobustFOV, ApplyXFM, ImageMaths
from nipype.interfaces.ants import N4BiasFieldCorrection
from nipype.interfaces.fsl.base import FSLCommand, FSLCommandInputSpec
from nipype.interfaces.base import (traits, TraitedSpec, OutputMultiPath, File,
CommandLine, CommandLineInputSpec, isdefined)
from .reports import SegmentationRPT
from .utils import extract_labels

class RobustFOVadvInputSpec(FSLCommandInputSpec):
    in_file = File(exists=True,
                   desc='input filename',
                   argstr='-i %s', position=0, mandatory=True)
    out_roi = File(desc="ROI volume output name", argstr="-r %s",
                   name_source=['in_file'], hash_files=False,
                   name_template='%s_ROI')
    out_mat = File(desc="ROI volume output matrix", argstr="-m %s",
                   name_source=['in_file'], hash_files=False,
                   name_template='%s_.mat')


class RobustFOVadvOutputSpec(TraitedSpec):
    out_roi = File(exists=True, desc="ROI volume output name")
    out_mat = File(exists=True, desc="ROI volume output matrix")


class RobustFOVadv(FSLCommand):
    """ Advanced RobustFov with output matrix """
    _cmd = 'robustfov'
    input_spec = RobustFOVadvInputSpec
    output_spec = RobustFOVadvOutputSpec



def brain_extraction_wf(report_dir='/tmp/wmi_workflow_report/'):
    """ Generate a Brain-Extraction-Workflow"""

    inputnode = Node(IdentityInterface(fields=["in_file"]), name='inputnode')
    n4correction = Node(N4BiasFieldCorrection(), name='n4correction')
    robustfov = Node(RobustFOVadv(), name='robustfov')
    bet = Node(BET(robust=True, mask=True, frac= 0.5), name='bet')
    warpback = Node(ApplyXFM(datatype='char', interp='nearestneighbour'), name='warpback')
    applymask = Node(ImageMaths(op_string='-mas'), name='applymask')
    outputnode = Node(IdentityInterface(fields=['mask', 'brain']), name='outputnode')
    subject_id_node = Node(extract_labels(), name='subject_id_node')
    subject_id_node.inputs.label = 'sub'
    reportnode = Node(SegmentationRPT(contrast='T1',
                                    command='Brainextraction',
                                    report_dir=report_dir),
                        name='reportnode')

    wf = Workflow(name='brain_extraction')

    wf.connect([(inputnode, n4correction, [('in_file', 'input_image')]),
                (n4correction, robustfov, [('output_image', 'in_file')]),
                (robustfov, bet, [('out_roi','in_file')]),
                (robustfov, warpback, [('out_mat', 'in_matrix_file')]),
                (bet, warpback, [('mask_file', 'in_file')]),
                (inputnode, warpback, [('in_file', 'reference')]),
                (warpback, outputnode, [('out_file', 'mask')]),
                (inputnode, applymask, [('in_file', 'in_file')]),
                (warpback, applymask, [('out_file', 'in_file2')]),
                (inputnode, subject_id_node, [('in_file',  'fname')]),
                (subject_id_node, reportnode, [('values', 'subject_id')]),
                (inputnode, reportnode, [('in_file', 'in_file')]),
                (warpback, reportnode, [('out_file', 'mask')]),
                (applymask, outputnode, [('out_file', 'brain')])
               ])

    return wf



def brain_extraction_spm_wf(report_dir='/tmp/wmi_workflow_report/'):
    """ Brain extraction Workflow using SPM for bias-field-correction
    instead of ANTs
    Can be  used as replacement for standard `brain_extraction_wf()`

    """

    from nipype.algorithms.misc import Gunzip
    from nipype.interfaces.spm import Segment
    from nipype.interfaces.fsl import BET, ApplyXFM
    from nipype import Workflow, Node, IdentityInterface



    inputnode = Node(IdentityInterface(fields=["in_file"]), name='inputnode')
    unzip = Node(Gunzip(), name='unzip')
    bias_correction = Node(Segment(bias_regularization=0.0001), name='bias_correction')
    bias_correction.inputs.save_bias_corrected = True
    robustfov = Node(RobustFOVadv(), name='robustfov')
    bet = Node(BET(robust=True, mask=True, frac= 0.5), name='bet')
    warpback = Node(ApplyXFM(datatype='char', interp='nearestneighbour'), name='warpback')
    applymask = Node(ImageMaths(op_string='-mas'), name='applymask')
    outputnode = Node(IdentityInterface(fields=['mask', 'brain']), name='outputnode')
    subject_id_node = Node(extract_labels(), name='subject_id_node')
    subject_id_node.inputs.label = 'sub'
    reportnode = Node(SegmentationRPT(contrast='T1',
                                      command='Brainextraction',
                                      report_dir=report_dir),
                      name='reportnode')

    wf = Workflow(name='brain_extraction_spm')
    wf.connect([(inputnode, unzip, [('in_file', 'in_file')]),
                (unzip, bias_correction, [('out_file', 'data')]),
                (bias_correction, robustfov, [('bias_corrected_image', 'in_file')]),
                (robustfov, bet, [('out_roi','in_file')]),
                (robustfov, warpback, [('out_mat', 'in_matrix_file')]),
                (bet, warpback, [('mask_file', 'in_file')]),
                (inputnode, warpback, [('in_file', 'reference')]),
                (warpback, outputnode, [('out_file', 'mask')]),
                (inputnode, applymask, [('in_file', 'in_file')]),
                (warpback, applymask, [('out_file', 'in_file2')]),
                (inputnode, subject_id_node, [('in_file',  'fname')]),
                (subject_id_node, reportnode, [('values', 'subject_id')]),
                (inputnode, reportnode, [('in_file', 'in_file')]),
                (warpback, reportnode, [('out_file', 'mask')]),
                (applymask, outputnode, [('out_file', 'brain')])
                ])
    return wf
