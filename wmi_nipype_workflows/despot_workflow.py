from nipype import Workflow, IdentityInterface, Node, Merge, MapNode, Function
from nipype.interfaces.fsl import MCFLIRT, FLIRT

from nipype.interfaces.fsl import Split as FslSplit
from nipype.interfaces.fsl import Merge as FslMerge
from nipype.interfaces.io import JSONFileGrabber

from .brainextract_workflow import brain_extraction_wf, brain_extraction_spm_wf

from nipype.interfaces.fsl.base import FSLCommand, FSLCommandInputSpec
from nipype.interfaces.base import (traits, TraitedSpec, InputMultiPath, File, Directory,
        CommandLine, CommandLineInputSpec, isdefined, CommandLine)

from pathlib import Path


""" The Workflows porovided by this module are used to prepare and Perform
mcdespot. First you might need to `prepare_mcdespot` this workflow does
motion-correction and skullstripping. Then you can connect it's outputs to
`run_mcdespot`

Currently there are 2 different mcdespot implementations supported:
    * pymcdespot
    * cmcdespot (classic_mcdespot)

"""

class ChangeFiletypeInputSpec(FSLCommandInputSpec):
    in_file = File(
            exists=True,
            desc='input filename',
            argstr='%s',
            position=1,
            mandatory=True)
    output_filetype = traits.Str(
            argstr="%s",
            position=0,
            desc="ouptut Filetype - ANALYZE, NIFTI or NIFTI_GZ",
            mandatory=True)
    out_file = File(
            desc='ouptut File',
            argstr='%s',
            position=-1,
            genfile=True
            )

class ChangeFiletypeOutputSpec(TraitedSpec):
    out_file = File(exists=True)

class ChangeFiletype(FSLCommand):
    """ Use FSLChfiletype command to change type of input_files
    """

    _cmd = 'fslchfiletype'

    input_spec = ChangeFiletypeInputSpec
    output_spec = ChangeFiletypeOutputSpec


    def _list_outputs(self):
        from pathlib import Path
        from os.path import abspath

        outputs = self._outputs().get()
        out_file = self.inputs.out_file
        in_file = self.inputs.in_file
        output_filetype= self.inputs.output_filetype

        if not isdefined(out_file):
            orig_suffix_list = Path(in_file).suffixes
            orig_suffix = ""
            for s in orig_suffix_list:
                orig_suffix += s

            suffixes = {'ANALYZE': 'img', 'NIFTI': 'nii', 'NIFTI_GZ': 'nii.gz'}
            suffix = suffixes[output_filetype]

            out_file = in_file.replace(orig_suffix, ".%s" %suffix)


        outputs['out_file'] = abspath(out_file)
        return outputs

    def _gen_filename(self, name):
        if name == 'out_file':
            return self._list_outputs()['out_file']
        return None


class Despot1HifiInputSpec(CommandLineInputSpec):
    despotSwitch = traits.Int(
        2,
        argstr="%i",
        position=0,
        desc="Which Version of Despot1 to use: 1:Despot1, 2:Despot1Hifi, 3:Despot1+B1,4:Despot1+precomputed RF-pulse",
        mandatory=True,
        usedefault=True)
    number_of_spgr = traits.Int(
        0,
        argstr="%i",
        position=1,
        mandatory=True,
        usedefault=True,
        desc="Number of spgr-files will be set automatically")
    in_spgr = InputMultiPath(
        File(exists=True),
        argstr="%s",
        position=3,
        mandatory=True,
        desc="List of SPGR-Files in ANALYZE-Format")
    spgr_flipangles = traits.List(
        traits.Float,
        argstr="%s",
        position=-14,
        mandatory=True)
    in_irspgr = File(exists=True,
        argstr="%s",
        position=-12,
        mandatory=True)
    spgr_tr = traits.Float(
        position=2,
        argstr="%0.1f",
        mandatory=True,
        desc="Repttition Time (TR) for SPGR-Images in ms"
        )
    irspgr_ti = traits.Float(
        mandatory=True,
        argstr="%0.0f",
        position=-11,
        desc="Inversion Time (TI)")
    irspgr_tr = traits.Float(
        mandatory=True,
        argstr="%0.1f",
        position=-10,
        desc="Repetition Time(TR) for irspgr-Image")
    irspgr_flipangle = traits.Float(
        mandatory=True,
        argstr="%0.1f",
        position=-9,
        default=5,
        desc="irspgr FLipangle")
    readout_pulses = traits.Int(
        mandatory=True,
        argstr="%i",
        default=100,
        position=-8)
    field_strength = traits.Float(
        mandatory=True,
        argstr="%0.1f",
        position=-7)
    inversion_mode = traits.Int(
        2,
        argstr="%i",
        position=-6,
        mandatory=True,
        usedefault=True)
    output_dir = Directory(
        ".",
        argstr="%s",
        position=-5,
        mandatory=True,
        usedefault=True)
    noiseThresholdScale = traits.Float(
        0,
        argstr="%0.1f",
        position=-4,
        mandatory=True,
        usedefault=True)
    smoothB1Field = traits.Bool(
        False,
        argstr="%i",
        position=-3,
        mandatory=True,
        usedefault=True)
    residualCheck = traits.Int(
        0,
        argstr="%i",
        position=-2,
        usedefault=True,
        mandatory=True)
    speckleThreshold = traits.Int(
        1,
        argstr="%i",
        position=-1,
        usedefault=True,
        mandatory=True)

class Despo1HifiOutputSpec(TraitedSpec):
    T1Map = File(desc="T1Map", exists=True)
    MoMap = File(desc="MoMap", exosts=True)
    B1Map = File(desc="B1Map", exists=True)
    ResidualMap = File(desc="ResidualMap", exists=True)



class Despot1Hifi(CommandLine):
    input_spec = Despot1HifiInputSpec
    output_spec = Despo1HifiOutputSpec

    _cmd = 'despot1'

    def _list_outputs(self):
        outputs = self.output_spec().get()
        outputs['T1Map'] = str(Path('DESPOT1HIFI_T1Map.img').absolute())
        outputs['MoMap'] = str(Path('DESPOT1HIFI_MoMap.img').absolute())
        outputs['B1Map'] = str(Path('DESPOT1HIFI_B1Map.img').absolute())
        outputs['ResidualMap'] =str(Path('DESPOT1HIFI_ResidualMap.img').absolute())
        return outputs

    def _format_arg(self, name, spec, value):
        print("Despot1 input")
        print(name)
        print(value)
        if name == "number_of_spgr":
            return super(Despot1Hifi, self)._format_arg(name, spec, len(self.inputs.in_spgr))
        if name == "spgr_tr":
            return super(Despot1Hifi, self)._format_arg(name, spec, value * 1000)
        if name == "output_dir":
            outdir = str(Path(value).absolute())
            return super(Despot1Hifi, self)._format_arg(name, spec, outdir)
        if name == "in_irspgr":
            filename = str(Path(value).with_suffix(''))
            return super(Despot1Hifi, self)._format_arg(name, spec, filename)
        if name == "in_spgr":
            filenames = [str(Path(fname).with_suffix('')) for fname in value]
            return super(Despot1Hifi, self)._format_arg(name, spec, filenames)

        return super(Despot1Hifi, self)._format_arg(name, spec, value)



class Despot2(CommandLine):

    _cmd = 'despot2'


def prepare_mcdespot(spm_biasfilter=False, use_twopool=False):
    """ prepare a mcdespot-workflow. If use_twopool is set to true, twopool
    mcdespot will be used, else threepool_mcdespot is used.

    Parameters
    ----------
    spm_biasfilter: bool
        Use SPM for bias-filtering instead of ants
    use_twopool: bool
        Use Twopool mcdespot instead of Threepool

    Returns
    ------

    nipype workflow

    """
    wf = prepare_twopool_mcdespot(spm_biasfilter) if use_twopool else prepare_threepool_mcdespot(spm_biasfilter)
    return wf


def prepare_twopool_mcdespot(spm_biasfilter=False):
    """ create a nipype workflow to prepare twopool mcdespot 

    Parameters
    ----------
    spm_biasfilter: bool
        Use SPM for bias-filtering instead of ants

    Returns
    -------

    nipype workflow

    """

    def _get_target(in_files):
        """ SPGR with highest FA is Target """
        return in_files[-1]


    inputnode = Node(IdentityInterface(fields=['in_spgr', 'in_ssfp']), name='inputnode')
    motion_correct_spgr = Node(MCFLIRT(), name="motion_correct_spgr")
    motion_correct_ssfp = Node(MCFLIRT(), name="motion_correct_ssfp")

    split_spgr = Node(FslSplit(dimension='t'), name="split_spgr")
    split_ssfp = Node(FslSplit(dimension='t'), name="split_ssfp")
    register_spgr = MapNode(FLIRT(), iterfield=["in_file"], name="register_spgr")
    register_ssfp = MapNode(FLIRT(), iterfield=["in_file"], name="register_ssfp")

    get_target = Node(Function(input_names=['in_files'],
                               output_names=['out_file'],
                               function=_get_target),
                      name='get_target')
    if spm_biasfilter:
        extract_brain = brain_extraction_spm_wf()
    else:
        extract_brain = brain_extraction_wf()

    merge_spgr = Node(FslMerge(dimension='t'), name='merge_spgr')
    merge_ssfp = Node(FslMerge(dimension='t'), name='merge_ssfp')

    outputnode = Node(IdentityInterface(fields=['spgr', 'ssfp', 'brainmask', 'target']), name='outputnode')

    wf = Workflow(name="prepare_mcdespot")
    wf.connect([(inputnode, motion_correct_spgr, [('in_spgr', 'in_file')]),
                (inputnode, motion_correct_ssfp, [('in_ssfp', 'in_file')]),
                (motion_correct_spgr, split_spgr, [('out_file', 'in_file')]),
                (motion_correct_ssfp, split_ssfp, [('out_file', 'in_file')]),
                (split_spgr, register_spgr, [('out_files', 'in_file')]),
                (split_ssfp, register_ssfp, [('out_files', 'in_file')]),
                (split_spgr, get_target, [('out_files', 'in_files')]),
                (get_target, register_spgr, [('out_file', 'reference')]),
                (get_target, register_ssfp, [('out_file', 'reference')]),
                (register_spgr, merge_spgr, [('out_file', 'in_files')]),
                (register_ssfp, merge_ssfp, [('out_file', 'in_files')]),
                (get_target, extract_brain, [('out_file', 'inputnode.in_file')]),
                (extract_brain, outputnode, [('outputnode.mask', 'brainmask')]),
                (get_target, outputnode, [('out_file', 'target')]),
                (merge_spgr, outputnode, [('merged_file', 'spgr')]),
                (merge_ssfp, outputnode, [('merged_file', 'ssfp')]),
               ])

    return wf



def prepare_threepool_mcdespot(spm_biasfilter=False):
    """ create a nipype workflow to prepare threepool mcdespot 

    Parameters
    ----------
    spm_biasfilter: bool
        Use SPM for bias-filtering instead of ants

    Returns
    -------

    nipype workflow

    """

    def _get_target(in_files):
        """ SPGR with highest FA is Target """
        return in_files[-1]

    inputnode = Node(IdentityInterface(fields=['in_irspgr', 'in_spgr', 'in_ssfp180', 'in_ssfp0']),
                     name="inputnode")
    motion_correct_spgr = Node(MCFLIRT(), name="motion_correct_spgr")
    motion_correct_ssfp0 = Node(MCFLIRT(), name="motion_correct_ssfp0")
    motion_correct_ssfp180 = Node(MCFLIRT(), name="motion_correct_ssfp180")
    split_spgr = Node(FslSplit(dimension='t'), name="split_spgr")
    split_ssfp0 = Node(FslSplit(dimension='t'), name="split_ssfp0")
    split_ssfp180 = Node(FslSplit(dimension='t'), name="split_ssfp180")
    register_irspgr = Node(FLIRT(), name='register_irspgr')

    register_spgr = MapNode(FLIRT(), iterfield=["in_file"], name="register_spgr")
    register_ssfp0 = MapNode(FLIRT(), iterfield=["in_file"], name="register_ssfp0")
    register_ssfp180 = MapNode(FLIRT(), iterfield=["in_file"], name="register_ssfp180")

    get_target = Node(Function(input_names=['in_files'],
                               output_names=['out_file'],
                               function=_get_target),
                      name='get_target')
    if spm_biasfilter:
        extract_brain = brain_extraction_spm_wf()
    else:
        extract_brain = brain_extraction_wf()

    merge_spgr = Node(FslMerge(dimension='t'), name='merge_spgr')
    merge_ssfp0 = Node(FslMerge(dimension='t'), name='merge_ssfp0')
    merge_ssfp180 = Node(FslMerge(dimension='t'), name='merge_ssfp180')

    outputnode = Node(IdentityInterface(fields=['irspgr', 'spgr', 'ssfp0', 'ssfp180', 'brainmask', 'target']), name='outputnode')

    wf = Workflow(name="prepare_mcdespot")
    wf.connect([(inputnode, motion_correct_spgr, [('in_spgr', 'in_file')]),
                (inputnode, motion_correct_ssfp0, [('in_ssfp0', 'in_file')]),
                (inputnode, motion_correct_ssfp180, [('in_ssfp180', 'in_file')]),
                (motion_correct_spgr, split_spgr, [('out_file', 'in_file')]),
                (motion_correct_ssfp0, split_ssfp0, [('out_file', 'in_file')]),
                (motion_correct_ssfp180, split_ssfp180, [('out_file', 'in_file')]),
                (inputnode, register_irspgr, [('in_irspgr', 'in_file')]),
                (split_spgr, register_spgr, [('out_files', 'in_file')]),
                (split_ssfp0, register_ssfp0, [('out_files', 'in_file')]),
                (split_ssfp180, register_ssfp180, [('out_files', 'in_file')]),
                (split_spgr, get_target, [('out_files', 'in_files')]),
                (get_target, register_irspgr, [('out_file', 'reference')]),
                (get_target, register_spgr, [('out_file', 'reference')]),
                (get_target, register_ssfp0, [('out_file', 'reference')]),
                (get_target, register_ssfp180, [('out_file', 'reference')]),
                (register_spgr, merge_spgr, [('out_file', 'in_files')]),
                (register_ssfp0, merge_ssfp0, [('out_file', 'in_files')]),
                (register_ssfp180, merge_ssfp180, [('out_file', 'in_files')]),
                (get_target, extract_brain, [('out_file', 'inputnode.in_file')]),
                (extract_brain, outputnode, [('outputnode.mask', 'brainmask')]),
                (get_target, outputnode, [('out_file', 'target')]),
                (register_irspgr, outputnode, [('out_file', 'irspgr')]),
                (merge_spgr, outputnode, [('merged_file', 'spgr')]),
                (merge_ssfp0, outputnode, [('merged_file', 'ssfp0')]),
                (merge_ssfp180, outputnode, [('merged_file', 'ssfp180')])
               ])

    return wf


def json_sidecar(paths):
    """ get json-sidecar-name of nifti-file """
    from pathlib import Path
    if not type(paths) is list:
        filenames = [paths]
    else:
        filenames = paths
    print("Got paths:")
    print(paths)

    jsonfiles = []
    for filename in filenames:
        fpath = Path(filename)
        suffix = "".join(fpath.suffixes)
        fname = fpath.name.replace(suffix,".json")
        jsonfiles.append(str(fpath.parent.joinpath(fname).absolute()))

    if len(jsonfiles) == 1:
        return jsonfiles[0]

    return jsonfiles

def run_classic_mcdespot(spm_biasfilter=True, ):
    """ This function creates a workflow to run the original mcdespot
    created by Deoni"""

    def _define_flipangles(flipangle):
        """ calculate flipangels """
        from numpy import array as nparray
        multiplier = [0.16666667, 0.22222222, 0.27777778, 0.33333333,
                      0.38888889, 0.5, 0.72222222, 1.0]
        flipangles = nparray(multiplier) * flipangle
        return list(flipangles)



    # The Inputnode is identical to run_mcdespot which uses opencl
    inputnode = Node(IdentityInterface(fields=['in_irspgr', 'in_spgr', 'in_ssfp180', 'in_ssfp0']),
                     name="inputnode")
    prep_mcdespot = prepare_mcdespot(spm_biasfilter)
    split_spgr = Node(FslSplit(dimension='t'), name='split_spgr')
    split_ssfp0 = Node(FslSplit(dimension='t'), name='split_ssfp0')
    split_ssfp180 = Node(FslSplit(dimension='t'), name='split_ssfp180')
    convert_irspgr = Node(ChangeFiletype(output_filetype='ANALYZE'),
            name='convert_irspgr')
    convert_spgr = MapNode(ChangeFiletype(output_filetype='ANALYZE'),
                           iterfield=['in_file'], name='convert_spgr')
    convert_ssfp0 = MapNode(ChangeFiletype(output_filetype='ANALYZE'),
                            iterfield=['in_file'], name='convert_ssfp0')
    convert_ssfp180 = MapNode(ChangeFiletype(output_filetype='ANALYZE'),
                              iterfield=['in_file'], name='convert_ssfp180')
    despot1 = Node(Despot1Hifi(), name="despot1")
    despot1.inputs.irspgr_ti = 450
    despot1.inputs.readout_pulses = 100
    despot1.inputs.field_strength = 3.0

    # Load Metadata from JSON-Sidecars
    spgr_metadata = Node(JSONFileGrabber(), name='spgr_metadata')
    ssfp_metadata = Node(JSONFileGrabber(), name='ssfp_metadata')
    irspgr_metadata = Node(JSONFileGrabber(), name='irspgr_metadata')
    # Fetch Flipangles from JSON-Files
    ssfp_flipangles = Node(Function(function=_define_flipangles,
        input_names=['flipangle'], output_names=['flipangles']),
                           name='ssfp_flipangles')
    spgr_flipangles = Node(Function(function=_define_flipangles,
        input_names=['flipangle'], output_names=['flipangles']),
                            name = 'spgr_flipangles')
    # Now Connect all Nodes
    wf = Workflow(name='run_mcdespot')

    wf.connect([(inputnode, ssfp_metadata, [(('in_ssfp0', json_sidecar), 'in_file')]),
                (inputnode, spgr_metadata, [(('in_spgr', json_sidecar), 'in_file')]),
                (inputnode, irspgr_metadata, [(('in_irspgr', json_sidecar), 'in_file')]),
                (inputnode, prep_mcdespot, [('in_irspgr', 'inputnode.in_irspgr'),
                                            ('in_spgr', 'inputnode.in_spgr'),
                                            ('in_ssfp180', 'inputnode.in_ssfp180'),
                                            ('in_ssfp0', 'inputnode.in_ssfp0')]),(split_spgr, convert_spgr, [('out_files', 'in_file')]),
                (prep_mcdespot, split_spgr, [('outputnode.spgr', 'in_file')]),
                (prep_mcdespot, split_ssfp0, [('outputnode.ssfp0', 'in_file')]),
                (prep_mcdespot, split_ssfp180, [('outputnode.ssfp180', 'in_file')]),
                (split_ssfp0, convert_ssfp0, [('out_files', 'in_file')]),
                (split_ssfp180, convert_ssfp180, [('out_files', 'in_file')]),
                (prep_mcdespot, convert_irspgr, [('outputnode.irspgr', 'in_file')]),
                (convert_spgr, despot1, [('out_file', 'in_spgr')]),
                (convert_irspgr, despot1, [('out_file', 'in_irspgr')]),
                (spgr_metadata, spgr_flipangles, [('FlipAngle', 'flipangle')]),
                (ssfp_metadata, ssfp_flipangles, [('FlipAngle', 'flipangle')]),
                (spgr_flipangles, despot1, [('flipangles', 'spgr_flipangles')]),
                (spgr_metadata, despot1, [('RepetitionTime', 'spgr_tr')]),
                (irspgr_metadata, despot1, [('FlipAngle', 'irspgr_flipangle'),
                                            ('RepetitionTime', 'irspgr_tr')])
                ])

    return wf


def run_mcdespot(spm_biasfilter=False, use_twopool=False):
    """ run mcdespot workflow

    Parameters
    ----------
    spm_biasfilter: bool
        Use SPM for biascorrection instead of ANTS N4, default: False
    use_twopool: bool
        Perform Twopool_mcdespot if set to true, else perform threepool_mcdespot

    Returns
    -------
    full mcdespot workflow
    """

    wf = run_twopool_mcdespot(spm_biasfilter) if use_twopool else run_threepool_mcdespot(spm_biasfilter)

    return wf




def run_twopool_mcdespot(spm_biasfilter=False):
    """ run mcdespot workflow 
    
    Parameters
    ----------

    spm_biasfilter: bool
        Use SPM for biascorrection instead of ANTS N4, default:False
    

    Returns
    -------

    nipype workflow 

    """
    import pymcdespot.nipype as mcdnp

    def _define_flipangles(flipangle):
        """ calculate flipangels """
        from numpy import array as nparray
        multiplier =[0.16666667, 0.22222222, 0.27777778, 0.33333333,
                     0.38888889, 0.5, 0.72222222, 1.0]
        flipangles = nparray(multiplier) * flipangle
        return list(flipangles)

    inputnode = Node(IdentityInterface(fields=['in_spgr', 'in_ssfp']), name="inputnode")
    prep_mcdespot = prepare_twopool_mcdespot(spm_biasfilter)
    mcdnode = Node(mcdnp.Twopool_mcdespot(), name="mcdnode")

    ssfp_flipangles = Node(Function(function=_define_flipangles,
                                    input_names=['flipangle'],
                                    output_names=['flipangles']),
                           name='ssfp_flipangles')
    spgr_flipangles = Node(Function(function=_define_flipangles,
                                    input_names=['flipangle'],
                                    output_names=['flipangles']),
                           name='spgr_flipangles')
    spgr_metadata = Node(JSONFileGrabber(), name='spgr_metadata')
    ssfp_metadata = Node(JSONFileGrabber(), name='ssfp_metadata')

    outputnode = Node(IdentityInterface(
        fields=['mwf', 'mrt', 'qT1', 'qT2', 'brainmask', 'target']),
            name='outputnode')

    wf = Workflow(name='run_mcdespot')

    wf.connect([(inputnode, ssfp_metadata, [(('in_ssfp', json_sidecar), 'in_file')]),
                (inputnode, spgr_metadata, [(('in_spgr', json_sidecar), 'in_file')]),
                (inputnode, prep_mcdespot, [('in_spgr', 'inputnode.in_spgr'),
                                            ('in_ssfp', 'inputnode.in_ssfp')]),
                (prep_mcdespot, mcdnode, [('outputnode.ssfp', 'ssfp_file'),
                                          ('outputnode.spgr', 'spgr_file'),
                                          ('outputnode.brainmask', 'mask')]),
                (spgr_metadata, spgr_flipangles, [('FlipAngle', 'flipangle')]),
                (ssfp_metadata, ssfp_flipangles, [('FlipAngle', 'flipangle')]),
                (spgr_flipangles, mcdnode, [('flipangles', 'spgr_fa')]),
                (ssfp_flipangles, mcdnode, [('flipangles', 'ssfp_fa')]),
                (spgr_metadata, mcdnode, [('RepetitionTime', 'spgr_tr')]),
                (ssfp_metadata, mcdnode, [('RepetitionTime', 'ssfp_tr')]),
                (mcdnode, outputnode, [('myelin_fraction_map', 'mwf'),
                                       ('mrt_map', 'mrt'),
                                       ('t1_map', 'qT1'),
                                       ('t2_map', 'qT2')]),
                (prep_mcdespot, outputnode, [('outputnode.brainmask', 'brainmask'),
                                             ('outputnode.target', 'target')])
               ])

    return wf





def run_threepool_mcdespot(spm_biasfilter=False):
    """ run mcdespot workflow 
    
    Parameters
    ----------

    spm_biasfilter: bool
        Use SPM for biascorrection instead of ANTS N4, default:False
    

    Returns
    -------

    nipype workflow 

    """
    import pymcdespot.nipype as mcdnp

    def _define_flipangles(flipangle):
        """ calculate flipangels """
        from numpy import array as nparray
        multiplier =[0.16666667, 0.22222222, 0.27777778, 0.33333333,
                     0.38888889, 0.5, 0.72222222, 1.0]
        flipangles = nparray(multiplier) * flipangle
        return list(flipangles)

    inputnode = Node(IdentityInterface(fields=['in_irspgr', 'in_spgr', 'in_ssfp180', 'in_ssfp0']),
                     name="inputnode")
    prep_mcdespot = prepare_threepool_mcdespot(spm_biasfilter)
    mcdnode = Node(mcdnp.Threepool_mcdespot(), name="mcdnode")
    mcdnode.inputs.ispgr_ti_times = 450

    ssfp_files = Node(Merge(2), name='ssfp_files')
    ssfp_flipangles = Node(Function(function=_define_flipangles, input_names=['flipangle'], output_names=['flipangles']),
                           name='ssfp_flipangles')
    spgr_flipangles = Node(Function(function=_define_flipangles, input_names=['flipangle'], output_names=['flipangles']),
                           name='spgr_flipangles')
    spgr_metadata = Node(JSONFileGrabber(), name='spgr_metadata')
    ssfp_metadata = Node(JSONFileGrabber(), name='ssfp_metadata')
    irspgr_metadata = Node(JSONFileGrabber(), name='irspgr_metadata')
    outputnode = Node(IdentityInterface(
            fields=['mwf', 'mrt', 'qT1', 'qT2', 'brainmask', 'target']),
            name='outputnode')

    wf = Workflow(name='run_mcdespot')

    wf.connect([(inputnode, ssfp_metadata, [(('in_ssfp0', json_sidecar), 'in_file')]),
                (inputnode, spgr_metadata, [(('in_spgr', json_sidecar), 'in_file')]),
                (inputnode, irspgr_metadata, [(('in_irspgr', json_sidecar), 'in_file')]),
                (inputnode, prep_mcdespot, [('in_irspgr', 'inputnode.in_irspgr'),
                                            ('in_spgr', 'inputnode.in_spgr'),
                                            ('in_ssfp180', 'inputnode.in_ssfp180'),
                                            ('in_ssfp0', 'inputnode.in_ssfp0')]),
                (prep_mcdespot, ssfp_files, [('outputnode.ssfp0', 'in1'),
                                             ('outputnode.ssfp180', 'in2')]),
                (ssfp_files, mcdnode, [('out', 'ssfp_files')]),
                (prep_mcdespot, mcdnode, [('outputnode.irspgr', 'ispgr_file'),
                                          ('outputnode.spgr', 'spgr_file'),
                                          ('outputnode.brainmask', 'mask')]),
                (spgr_metadata, spgr_flipangles, [('FlipAngle', 'flipangle')]),
                (ssfp_metadata, ssfp_flipangles, [('FlipAngle', 'flipangle')]),
                (spgr_flipangles, mcdnode, [('flipangles', 'spgr_fa')]),
                (ssfp_flipangles, mcdnode, [('flipangles', 'ssfp_fa')]),
                (irspgr_metadata, mcdnode, [('FlipAngle', 'ispgr_fa'),
                                            ('PhaseEncodingSteps', 'phase_encoding'),
                                            ('RepetitionTime', 'ispgr_tr')]),
                (spgr_metadata, mcdnode, [('RepetitionTime', 'spgr_tr')]),
                (ssfp_metadata, mcdnode, [('RepetitionTime', 'ssfp_tr')]),
                (mcdnode, outputnode, [('myelin_fraction_map', 'mwf'),
                                       ('mrt_map', 'mrt'),
                                       ('t1_map', 'qT1'),
                                       ('t2_map', 'qT2')]),
                (prep_mcdespot, outputnode, [('outputnode.brainmask', 'brainmask'),
                                             ('outputnode.target', 'target')])
               ])

    return wf

def run_plain_mcdespot(layout, subjects=None, sessions=None, multiplier=None):
    """ Run mcdespot without preprocessing. This implies you have prepared
    all files. This workfflow fetches its input_files from bids-dataset. 

    Following files are used from bids-dataset:
        * SPGR-Image (plain as from Scanner) 4D-File
            - acq=SPGR
            - suffix=T1w
        * irspgr-Image (plain as from Scanner) 4D-File
            - acq=irspgr
            - suffix=


    Parameters
    ----------

    layout: BIDSLayout
        Dataset layout
    multiplier: list of float
        Multiplier to calculate Flipangles from json-sidecar
    subjects: list of str
        Select the subject where despot should be run onto
    

    """

    


    mcdnode = Node(mcdnp.Threepool_mcdespot(), name="mcdnode")
    mcdnode.inputs.ispgr_ti_times = 450

    
