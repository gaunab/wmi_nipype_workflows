#! env pytjhon

# ~*~ coding: utf-8 ~*~

import os
import nipype.interfaces.io as nio
os.environ["FSLDIR"] = "/usr/share/fsl/5.0/"
from nipype.interfaces.fsl import Merge as FslMerge
from nipype.interfaces.utility.base import Merge
from nipype import Node, Workflow, IdentityInterface
from nipype.interfaces.fsl.maths import MathsCommand, MeanImage, StdImage, MultiImageMaths
from nipype.interfaces import Function


def cov_wf():
    """ cov_wf
        calculate coefficient of variation (CoV) of longitudinal registered images of the same subject
        CoV=std_img/mean_img
        
    Parameters
    ----------
    
    Inputs
    -------
    in_files: str
        list of filename of baseline and registered Map (e.g. mwf_follow_up.nii.gz)
    
    Outputs
    -------
    out_file: str
        Filename of the resulting cov_img
        
    
    """
    ##NODES
    inputnode= Node(IdentityInterface( fields=["in_files"]), name="inputnode")
    
    fsl_merge= Node(FslMerge(dimension='t'), name= "fsl_merge") 

    mean_img= Node(MeanImage(), name='mean_img')

    std_img= Node(StdImage(), name='std_img')
    cov_img= Node(MultiImageMaths(), name='cov_img')
    cov_img.inputs.op_string = "-div %s "
    merge_operand_files= Node(Merge(1), name= "merge_operand_files") # make list for operand_files
    outputnode= Node(IdentityInterface(fields=["out_file"]), name="outputnode")
    
    ##WORKFLOWS
    cov_wf = Workflow(name="cov_wf")

    cov_wf.connect([(inputnode, fsl_merge, [('in_files', 'in_files')]),
                    (fsl_merge, mean_img, [('merged_file', 'in_file')]),
                    (fsl_merge, std_img, [('merged_file', 'in_file')]),
                    (std_img, cov_img, [('out_file', 'in_file')]), 
                    (mean_img, merge_operand_files,  [('out_file', 'in1')]),
                    (merge_operand_files, cov_img,  [('out', 'operand_files')]),
                    (cov_img, outputnode,  [('out_file', 'out_file')]),
                    ])

    return cov_wf



def get_roi_means(mapfile, roifile, output='means.tsv'):    
    """ Caclulate the means of all given rois in the mapfile
    
    Parameters
    ----------
    mapfile: str
        Filename of the Map (e.g. mwf.nii.gz)
    roifile: str
        Filename of the Image containing all rois
    output: str
        Filename of the resulting tsv (default: means.tsv)
        
    """
    from os.path import abspath
    import nibabel as nib
    import numpy as np
  
    
    output = abspath(output)
    data = nib.load(mapfile).get_data()
    roi_data = nib.load(roifile).get_data()
    labels = list(set(roi_data.flatten()))

    stats = []

    for lbl in labels:
        
        mean= np.mean(data[roi_data == lbl])
        std = np.std(data[roi_data == lbl])
        volume = (roi_data[roi_data == lbl] > 0).astype(np.int).sum()
        stats.append([lbl, mean, std, volume])

    np.savetxt(output, np.array(stats), delimiter='\t', header='lbl\tmean\tstd\tvolume', encoding='utf-8')

    return output, stats


    