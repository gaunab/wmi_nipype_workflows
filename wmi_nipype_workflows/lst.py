#! env python
# -*- coding: utf-8 -*-

__author__ = "Paul Kuntke"
__email__ = "paul.kuntke@ukdd.de"
__license__ = "BSD-3-Clause"

"""
Aeccess the Lesion segmentation toolbox for SPM12 with nipype


"""


from nipype.interfaces.spm.base import SPMCommand, SPMCommandInputSpec, ImageFileSPM, scans_for_fname
from nipype.interfaces.base import File, TraitedSpec, isdefined, traits
from nipype.utils.filemanip import ensure_list, fname_presuffix

class LstLPAInputSpec(SPMCommandInputSpec):
    flair = ImageFileSPM(
            exists=True,
            field="lpa.data_F2",
            mandatory=True,
            desc="FLAIR image"
            )
    reference = ImageFileSPM(
            exists=True,
            field="lpa.data_coreg",
            mandatory=False,
            desc="T1 for registration image"
            )
    report = traits.Bool(
            False,
            usedefault=True,
            field="lpa.html_report",
            desc="generate a html-report"
            )

class LstLPAOutputSpec(TraitedSpec):
    bias_corrected_flair = File(desc='bias corrected version of FLAIR image')
    coregistered_flair = File(desc='bias corrected and coregistered version of FLAIR image')
    lesion_propability_map = File(desc='Lesion propability map')

class LstLPA(SPMCommand):
    """
    Run Lesion Segmentation Toolbox from SPM
    """

    input_spec = LstLPAInputSpec
    output_spec = LstLPAOutputSpec

    def __init__(self, **inputs):
        self._jobtype = "tools"
        self._jobname = "LST"

        SPMCommand.__init__(self, **inputs)

    def _format_arg(self, opt, spec, val):
        if opt in ["flair", "reference"]:
            return scans_for_fname(ensure_list(val))

        return super(LstLPA, self)._format_arg(opt, spec, val)

    def _list_outputs(self):
        outputs = self._outputs().get()
        f = self.inputs.flair

        outputs["bias_corrected_flair"] = fname_presuffix(f, prefix="m")
        outputs["lesion_propability_map"] = fname_presuffix(f, prefix="ples_lpam")

        if (isdefined(self.inputs.reference)):
            outputs["coregistered_flair"] = fname_presuffix(f, prefix="mr")
            outputs["coregistered_lpm"] = fname_presuffix(f, "ples_lpamr")


        return outputs


class LstLGAInputSpec(SPMCommandInputSpec):
    t1 = ImageFileSPM(
            exists=True,
            field="lga.data_T1",
            mandatory=True,
            desc="T1 image"
            )
    flair = ImageFileSPM(
            exists=True,
            field="lga.data_F2",
            mandatory=True,
            desc="FLAIR image"
            )
    report = traits.Bool(
            False,
            usedefault=True,
            field="lpa.html_report",
            desc="generate a html-report")
    initial =  traits.Float( 0.3,
            usedefault=True,
            field="lga.opts_lga.initial",
            desc="Initial threshold"
            )
    mrf_parameter =  traits.Int(
            1,
            usedefault=True,
            field="lga.opts_lga.mrf",
            desc="Parameter for the Marjov Random Field"
            )
    maxiter =  traits.Int(
            50,
            usedefault=True,
            field="lga.opts_lga.maxiter",
            desc="Parameter for the Markov Random Field"
            )


class LstLGAOutputSpec(TraitedSpec):
    bias_corrected_flair = File(desc='bias corrected and with T1 coregistered version of FLAIR image')
    lesion_propability_map = File(desc='Lesion propability map')

class LstLGA(SPMCommand):
    """
    Lesion Growth algorithm  (LGA) -  Lesion Segmentation Toolbox from SPM
    """

    input_spec = LstLGAInputSpec
    output_spec = LstLGAOutputSpec

    def __init__(self, **inputs):
        self._jobtype = "tools"
        self._jobname = "LST"

        SPMCommand.__init__(self, **inputs)

    def _format_arg(self, opt, spec, val):
        if opt in ["flair", "t1"]:
            return scans_for_fname(ensure_list(val))

        return super(LstLGA, self)._format_arg(opt, spec, val)

    def _list_outputs(self):
        outputs = self._outputs().get()
        f = self.inputs.flair
        thresh = self.inputs.initial

        outputs["bias_corrected_flair"] = fname_presuffix(f, prefix="m")
        outputs["lesion_propability_map"] = fname_presuffix(f, prefix=f"ples_lga_{thresh}_rm")


        return outputs


