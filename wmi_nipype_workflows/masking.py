#! python
#
# ~*~ coding: utf-8 ~*~

__author__ = "Paul Kuntke"
__email__ = "paul.kuntke@ukdd.de"
__license__ = "BSD-3-Clause"

"""
Masking for work with atlases

"""

def seperate_labels(in_file, labels=None, out_basename=None):
    """ Seperate all given labels in an image. If no labels are given, all valid labels (except 0) are
    retrieved.

    Parameters
    ----------
    in_file: str
        Filename of Atlas
    labels: int or list of int
        Label or labels which should be extracted. If None is given, all valid labels are used
    out_basename: str
        basename of outptut_files. Label_number will be added before extension

    Returns
    -------

    Filenames of Outputs

    """

    def as_list(i):
        if type(i) is list:
            return i
        else:
            return [i]

    from os.path import abspath, extsep, basename
    from numpy import zeros, unique
    from nibabel import load, save, Nifti1Image

    atlas = load(in_file)
    atlas_data = atlas.get_data().astype(int)

    out_basename = out_basename if out_basename is not None else basename(in_file)
    out_prefix = out_basename.split(extsep)[0]
    out_suffix = out_basename.split(extsep)[1:]
    if len(out_suffix ) == 0:
        out_suffix = ['nii','gz']
    out_suffix = '.'.join(out_suffix)
    # Make sure labels is a list
    try:
        labels = list(unique(atlas_data)) if labels is None else as_list(labels)
    except:
        import ipdb
        ipdb.set_trace()

    outfiles = []

    for label in labels:
        out_data = zeros(atlas_data.shape)
        out_data[atlas_data == label] = 1
        out_file = abspath(''.join([out_prefix,'_',str(label),'.', out_suffix]))
        outfiles.append(out_file)
        save(Nifti1Image(out_data, atlas.affine, header=atlas.header), out_file)

    return outfiles

def seperate_dict_labels(in_dict):
    """ Run `seperate_labels` by inserting a single dict. The dict consists of the filename and 

    Parameters
    ----------
    in_dict: dictionary
        {file: 'filename', label: [labels]}

    Returns
    -------

    Filenames of outputs

    """
    from wmi_nipype_workflows.masking import seperate_labels

    out_files = seperate_labels(in_dict['file'], labels=in_dict['label'])

    return out_files


def label_seperator_interface():
    """ Create a nipype_Function-Interface to extract labels from a given atlas 

    Interface-Inputs:
        in_file: str
            atlas-filename
        labels: int or list of ints
            Choose the labels you want to extract
        out_basename:
            Give a basename for the output_files
    """

    from nipype.interfaces.utility import Function

    fn = Function(function=seperate_labels,
                  input_names=['in_file', 'labels', 'out_basename'],
                  output_names=['out_files'])
    return fn

def label_seperator_dict_interface():
    """ Create a nipype_Function-Interface to extract labels from a given atlas 

    Interface-Inputs:
        in_file: str
            atlas-filename
        labels: int or list of ints
            Choose the labels you want to extract
        out_basename:
            Give a basename for the output_files
    """

    from nipype.interfaces.utility import Function

    fn = Function(function=seperate_dict_labels,
                  input_names=['in_dict'],
                  output_names=['out_files'])
    return fn

