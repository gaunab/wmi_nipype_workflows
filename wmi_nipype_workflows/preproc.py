#! env pytjhon

# ~*~ coding: utf-8 ~*~

from nipype import Node, IdentityInterface, Workflow, Function

import os
os.environ["FSLDIR"] = "/usr/share/fsl/5.0/"
from nipype.interfaces.ants import N4BiasFieldCorrection
from nipype.interfaces.fsl.maths import MathsCommand, Threshold, MultiImageMaths, ApplyMask, UnaryMaths
from nipype.interfaces.spm import NewSegment
from nipype.algorithms.misc import Gunzip
from nipype.interfaces.utility import Merge, Split
from .image_statistik import get_roi_means



def bias_corr_wf():
    """ bias_corr_wf
        N4BiasFieldCorrection using ANTs 
        t1w, flair and spgr images are corrected

    """
    ##NODES
    inputnode= Node(IdentityInterface( fields=["flair","t1w", "spgr_target"]), name="inputnode")
    # Biascorrection for FLAIR  
    flair_BiasCorrection = Node(N4BiasFieldCorrection(), name="flair_BiasCorrection" )
    #T1w
    t1w_BiasCorrection = Node(N4BiasFieldCorrection(), name="t1w_BiasCorrection")
    #SPGR
    spgr_target_BiasCorrection = Node(N4BiasFieldCorrection(), name="spgr_BiasCorrection" )
    #outputnode
    outputnode=Node(IdentityInterface(fields=["flair_out_file", "t1w_out_file", "spgr_out_file"]), name="outputnode")

    ## WORKFLOWS # N4BiasFieldCorrection ants
    bias_corr = Workflow(name="bias_corr")
    #FLAIR
    bias_corr.connect(inputnode, "flair", flair_BiasCorrection, "input_image")
    bias_corr.connect(flair_BiasCorrection, "output_image", outputnode,"flair_out_file")
    #T1w
    bias_corr.connect(inputnode, "t1w", t1w_BiasCorrection, "input_image")
    bias_corr.connect(t1w_BiasCorrection, "output_image", outputnode,"t1w_out_file")
    #SPGR
    bias_corr.connect(inputnode, "spgr_target", spgr_target_BiasCorrection, "input_image") 
    bias_corr.connect(spgr_target_BiasCorrection, "output_image", outputnode,"spgr_out_file") 
    return bias_corr

def bias_corr_pluspd_wf():
    """ bias_corr_pluspd_wf
        N4BiasFieldCorrection using ANTs 
        t1w, flair, pd and spgr pd images are corrected

    """
    ##NODES
    inputnode= Node(IdentityInterface( fields=["flair","t1w", "pd", "spgr_target"]), name="inputnode")
    # Biascorrection for FLAIR  
    flair_BiasCorrection = Node(N4BiasFieldCorrection(), name="flair_BiasCorrection" )
    #T1w
    t1w_BiasCorrection = Node(N4BiasFieldCorrection(), name="t1w_BiasCorrection")
    #PD
    pd_BiasCorrection = Node(N4BiasFieldCorrection(), name="pd_BiasCorrection")
    #SPGR
    spgr_target_BiasCorrection = Node(N4BiasFieldCorrection(), name="spgr_BiasCorrection" )
    #outputnode
    outputnode=Node(IdentityInterface(fields=["flair_out_file", "t1w_out_file", "pd_out_file", "spgr_out_file"]), name="outputnode")

    ## WORKFLOWS # N4BiasFieldCorrection ants
    bias_corr_pluspd_wf= Workflow(name="bias_corr_pluspd_wf")
    #FLAIR
    bias_corr_pluspd_wf.connect(inputnode, "flair", flair_BiasCorrection, "input_image")
    bias_corr_pluspd_wf.connect(flair_BiasCorrection, "output_image", outputnode,"flair_out_file")
    #T1w
    bias_corr_pluspd_wf.connect(inputnode, "t1w", t1w_BiasCorrection, "input_image")
    bias_corr_pluspd_wf.connect(t1w_BiasCorrection, "output_image", outputnode,"t1w_out_file")
    #PD
    bias_corr_pluspd_wf.connect(inputnode, "pd", pd_BiasCorrection, "input_image")
    bias_corr_pluspd_wf.connect(pd_BiasCorrection, "output_image", outputnode,"pd_out_file")
    #SPGR
    bias_corr_pluspd_wf.connect(inputnode, "spgr_target", spgr_target_BiasCorrection, "input_image") 
    bias_corr_pluspd_wf.connect(spgr_target_BiasCorrection, "output_image", outputnode,"spgr_out_file") 
    return bias_corr_pluspd_wf    

def bias_corr_wf_allgemein():
    """ bias_corr_wf
        N4BiasFieldCorrection using ANTs 
        t1w, flair and spgr images are corrected

    """
    ##NODES
    inputnode= Node(IdentityInterface( fields=["in_file"]), name="inputnode")
    # Biascorrection for any image modality
    BiasCorrection = Node(N4BiasFieldCorrection(), name="BiasCorrection" )
    #outputnode
    outputnode=Node(IdentityInterface(fields=["out_file"]), name="outputnode")

    ## WORKFLOWS # N4BiasFieldCorrection ants
    bias_corre = Workflow(name="bias_corre")
    #any image
    bias_corre.connect(inputnode, "in_file", BiasCorrection, "input_image")
    bias_corre.connect(BiasCorrection, "output_image", outputnode,"out_file")
    
    return bias_corre

def tissue_seg_wf():
    """ tissue_seg_wf 
        Tissue Sementation WF using SPM NewSegment to gain wm, gm, csf probability maps (native_class_images) in native space
        catch wm, gm, csf probability map of native class image list
        apply threshold to probability map
        make binary image of thresholded probability map
    """
    inputnode= Node(IdentityInterface( fields=["in_file"]), name="inputnode")
    gunzip = Node(Gunzip(), name="gunzip")
    tissue_seg=Node(NewSegment(), name="tissue_seg")
    tissue_seg.inputs.channel_info = (0.0001, 60, (True, True))
    tissue_seg.inputs.affine_regularization="mni"

    def _get_gm(in_gm):
            """ select gm file from native_class_images list """
            return in_gm[0][0] #entspricht c1
    gm_inputnode = Node(IdentityInterface(fields='in_gm'), name="gm_inputnode")
    get_gm = Node(Function(input_names='in_gm', output_names='out_gm', function=_get_gm), name='get_gm')

    def _get_wm(in_wm):
            """ select wm file from native_class_images list """
            return in_wm[1][0] #entspricht c2
    wm_inputnode = Node(IdentityInterface(fields='in_wm'), name="wm_inputnode")
    get_wm = Node(Function(input_names='in_wm', output_names='out_wm', function=_get_wm), name='get_wm')

    def _get_csf(in_csf):

            """ select csf file from native_class_images list """
            return in_csf[2][0] # entspricht c3
    csf_inputnode = Node(IdentityInterface(fields='in_wm'), name="csf_inputnode")
    get_csf = Node(Function(input_names='in_csf', output_names='out_csf', function=_get_csf), name='get_csf')

    #threshold prob maps
    # (individual nodes were used instead of join node for WM,GM,CSF)
    thresh = .95 # define threshold for probability maps
    wm_thr=Node(Threshold(thresh=0.8),name="wm_thr")
    gm_thr=Node(Threshold(thresh=thresh),name="gm_thr")
    csf_thr=Node(Threshold(thresh=thresh),name="csf_thr")
    # make binary img of prob maps
    wm_make_bin_img=Node(MathsCommand(args="-bin"),name="wm_make_bin_img")
    gm_make_bin_img=Node(MathsCommand(args="-bin"),name="gm_make_bin_img")
    csf_make_bin_img=Node(MathsCommand(args="-bin"),name="csf_make_bin_img")

    outputnode=Node(IdentityInterface(fields=["wm_out_file", "gm_out_file", "csf_out_file"]), name="outputnode")

    ## WORKFLOWS
    segment = Workflow(name="tissue_seg")

    segment.connect([(inputnode, gunzip, [("in_file", "in_file")]),
                     (gunzip, tissue_seg, [("out_file","channel_files")]),
                     (tissue_seg, get_wm, [("native_class_images", "in_wm")]),
                     (tissue_seg, get_gm, [("native_class_images", "in_gm")]),
                     (tissue_seg, get_csf, [("native_class_images", "in_csf")]),
                     (get_wm, wm_thr, [("out_wm", "in_file")]),
                     (get_gm, gm_thr, [("out_gm", "in_file")]),
                     (get_csf, csf_thr, [("out_csf", "in_file")]),
                     (wm_thr, wm_make_bin_img, [("out_file", "in_file")]),
                     (gm_thr, gm_make_bin_img, [("out_file", "in_file")]),
                     (csf_thr, csf_make_bin_img, [("out_file", "in_file")]),
                     (wm_make_bin_img, outputnode, [("out_file", "wm_out_file")]),
                     (gm_make_bin_img, outputnode, [("out_file", "gm_out_file")]),
                     (csf_make_bin_img, outputnode, [("out_file", "csf_out_file")]),                 
                   ])
    return segment 
	
def calc_dawm_thresh(inlist, sd_multiplier=2):
    """
    Calculate the threshold value for dawm. Dawm is defined by sd_multiplier (default: 2)

    Parameters:
    -----------

    inlist: list of float
        mean, sd 

    Returns:
    --------
    float: threshold value

    """
    thresh = inlist[1] + sd_multiplier * inlist[2]
    return thresh

def remove_small_dawm( dawm_file, voxel_thresh=10): 
    from skimage.measure import label, regionprops
    from os.path import abspath
    import numpy as np 
    import nibabel as nib

    # label image regions
    #label_dawm_image=np.zeros(nib.load(dawm_file).shape)
    dawm = nib.load(dawm_file)
    dawm_img = nib.load(dawm_file).get_data()
    label_dawm_image, num= label(dawm_img, return_num=True)
    
    for region in regionprops(label_dawm_image):
        # take regions with large enough areas
        if region.area < voxel_thresh:
           x_1=np.where(label_dawm_image==region.label)
           #print('x_1')
           label_dawm_image[x_1]=0         
    
    out_file = abspath('label_dawm_image_thr3.nii.gz')
    nib.save(nib.Nifti1Image(label_dawm_image, dawm.affine, header=dawm.header), out_file)
    
    label_dawm_image[label_dawm_image != 0] = 1
    out_file_bin = abspath('dawm.nii.gz')
    nib.save(nib.Nifti1Image(label_dawm_image, dawm.affine, header=dawm.header), out_file_bin)
    return out_file, out_file_bin

def dawm_wf():	
    """ dawm_wf 
        Signal intensities of Flair image(without lesions) get thresholded by mean SI WM + 2SD SI WM 
		dawm get labels, dawm<10Voxel is removed in dawm_image	
    """
    ## NODES
    #inputnode = Node(IdentityInterface(fields=['wm', 't2l', 'in_file', 'threshold']), name='inputnode')
    inputnode = Node(IdentityInterface(fields=['wm', 't2l', 'in_file']), name='inputnode')
    merge_list = Node(Merge(1), name= 'merge_list')
    wm_ohne_t2l = Node(MultiImageMaths(), name='wm_ohne_t2l')
    wm_ohne_t2l.inputs.op_string = " -sub %s "
    thresh_wm_o_t2l = Node(Threshold(), name='thresh_wm_o_t2l')
    thresh_wm_o_t2l.inputs.thresh = 0
    roi_means = Function(function=get_roi_means, input_names=['mapfile', 'roifile'], output_names=['output', 'stats_array'])
    roimeans = Node(roi_means, name='roimeans')
    flair_masked = Node( ApplyMask(), name= 'flair_masked')
    # split stats array into 2 lists
    split_array = Node(Split(splits=[1,1], squeeze=True), name= 'split_array')
    # define_SI threshold for DAWM FLAIR image= mean SI WM + 2SD
    dawm_thresh = Function(function=calc_dawm_thresh, input_names=['inlist', 'sd_multiplier'], output_names=['thresh'])
    dawmthresh = Node(dawm_thresh, name= 'dawmthresh')
    thresh_flair = Node(Threshold(), name='thresh_flair')
    bin_threshflair = Node(UnaryMaths(), name='bin_threshflair')
    bin_threshflair.inputs.operation = 'bin'
    rm_sm_dawm = Function(function=remove_small_dawm, input_names=['in_file', 'dawm_file', 'voxel_thresh'], output_names=['out_file', 'bin_file'])
    rmsmdawm = Node(rm_sm_dawm, name= 'rmsmdawm')
    outputnode = Node(IdentityInterface(fields=['out_file']), name='outputnode')
	
	## WORKFLOWS
    get_dawm_mask = Workflow(name="get_dawm_mask")

    get_dawm_mask.connect([(inputnode, merge_list, [("t2l", "in1")]),
                (inputnode, wm_ohne_t2l, [("wm", "in_file")]),
                (merge_list, wm_ohne_t2l, [("out", "operand_files")]),
                (wm_ohne_t2l, thresh_wm_o_t2l , [("out_file", "in_file")]),           
                (thresh_wm_o_t2l, roimeans , [("out_file", "roifile")]),
                (inputnode, roimeans , [("in_file", "mapfile")]),
                (roimeans, split_array , [("stats_array", "inlist")]),   
                (split_array, dawmthresh , [("out2", "inlist")]),					 
                (thresh_wm_o_t2l, flair_masked , [("out_file", "mask_file")]),
                (inputnode, flair_masked , [("in_file", "in_file")]),   						   
                (dawmthresh, thresh_flair, [("thresh", "thresh")]),
                (flair_masked, thresh_flair, [("out_file", "in_file")]),           
                (thresh_flair, bin_threshflair, [("out_file", "in_file")]), 
                (bin_threshflair, rmsmdawm, [("out_file", "dawm_file")]),					   
                (rmsmdawm, outputnode , [("bin_file", "out_file")]),
                ])
			   
    return get_dawm_mask
