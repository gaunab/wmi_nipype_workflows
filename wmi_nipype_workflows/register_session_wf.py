#! env pytjhon

# ~*~ coding: utf-8 ~*~

from nipype import Node, IdentityInterface, Workflow

import os
os.environ["FSLDIR"] = "/usr/share/fsl/5.0/"
os.environ["ANTSPATH"] = "/usr/lib/ants/"
from nipype.interfaces.fsl import FLIRT,FNIRT, ApplyXFM, BET, ApplyWarp
from nipype.interfaces.fsl.maths import MathsCommand, Threshold
from nipype.interfaces.ants import N4BiasFieldCorrection
from nipype.interfaces.ants.registration import RegistrationSynQuick
from nipype.interfaces.ants.resampling import ApplyTransforms
from nipype.interfaces.utility.base import Merge


def reg_ses_wf():
    """ reg_ses_wf
        register sessions using FSL flirt (within session use affine matrix )
        t1w, flair and mwf images are warped to session target image

    """
	##NODES
    #inputnode mit out_put load_files verknüpfen
    inputnode= Node(IdentityInterface( fields=["flair","t1w", "mwf", "spgr_target"]), name="inputnode")
        #FLIRT target registration within session
    flair_coreg=Node(FLIRT(),name='flair_coreg')
    t1w_coreg=Node(FLIRT(),name='t1w_coreg')
    #outputnode
    outputnode=Node(IdentityInterface(fields=["flair_out_file","flair_out_matrix_file","t1w_out_file","t1w_out_matrix_file","mwf_out_file","mwf_out_matrix_file"]),name="outputnode")
	
	##WORKFLOWS
    reg_ses = Workflow(name="reg_ses")
    ## FLIRT
    #FLAIR
    reg_ses.connect(inputnode, "spgr_target",flair_coreg, "reference") 
    reg_ses.connect(inputnode, "flair", flair_coreg, "in_file")
    reg_ses.connect(flair_coreg,"out_file", outputnode,"flair_out_file")
    reg_ses.connect(flair_coreg,"out_matrix_file", outputnode,"flair_out_matrix_file")
    
    #T1w
    reg_ses.connect(inputnode, "spgr_target",t1w_coreg, "reference") 
    reg_ses.connect(inputnode, "t1w", t1w_coreg, "in_file")
    reg_ses.connect(t1w_coreg,"out_file", outputnode,"t1w_out_file")
    reg_ses.connect(t1w_coreg,"out_matrix_file", outputnode,"t1w_out_matrix_file")
    return reg_ses

def ants_reg_ses_wf():
    """ ants_reg_ses_wf
        register sessions using ANTs (within session use affine matrix by choosing transform_type="a )
        t1w, flair and mwf images are warped to session target image

    """
    ##NODES
    inputnode= Node(IdentityInterface( fields=["flair","t1w", "spgr_target"]), name="inputnode")
    flair_coreg=Node(RegistrationSynQuick(transform_type="a"),name='flair_coreg')   
    t1w_coreg=Node(RegistrationSynQuick(transform_type="a"),name='t1w_coreg')  
    #outputnode
    outputnode=Node(IdentityInterface(fields=["flair_out_file","flair_out_matrix_file","t1w_out_file","t1w_out_matrix_file"]),name="outputnode")
    ##WORKFLOWS
    ants_reg_ses = Workflow(name="ants_reg_ses")

    ants_reg_ses.connect([(inputnode, flair_coreg, [("spgr_target", "fixed_image"),
                                           ("flair", "moving_image")]),
                 (flair_coreg, outputnode, [("warped_image", "flair_out_file"),
                                            ("out_matrix", "flair_out_matrix_file")]),
                 (inputnode, t1w_coreg, [("spgr_target", "fixed_image"),
                                           ("t1w", "moving_image")]),
                 (t1w_coreg, outputnode, [("warped_image", "t1w_out_file"),
                                          ("out_matrix", "t1w_out_matrix_file")]),
                ])
    return ants_reg_ses

def reg_longi_wf():
    """ reg_longi_wf 
        register sessions lonitudinal using FSL flirt (affine matrix) & fnirt 
        input images must be 'preregistered' in same session space 
        t1w, flair and mwf images are warped to baseline target image

    """
    ##NODES
    #inputnode (biascorected spgr images, registered images to session session template) mit outputnode register_session_wf verknüpfen
    inputnode= Node(IdentityInterface( fields=["flair","t1w", "mwf", "target_0m", "target_followup"]), name="inputnode")
    #FLIRT target registration session12m to 0m
    flirt_followupto_0m=Node(FLIRT(),name='flirt_followupto_0m')
    #FNIRT
    fnirt_followupto_0m=Node(FNIRT(),name='fnirt_followupto_0m')
    #ApplyWarp (for nonlinear FNIRT) to follow-up dataset 
    t1w_applyw=Node(ApplyWarp(),name='t1w_applyw')
    flair_applyw=Node(ApplyWarp(),name='flair_applyw')
    mwf_applyw=Node(ApplyWarp(),name='mwf_applyw')
    #outputnode
    outputnode=Node(IdentityInterface(fields=["flair_out_file","t1w_out_file","mwf_out_file"]),name="outputnode")

    ## WORKFLOWS
    reg_longi = Workflow(name="reg_longi") 
    reg_longi.connect([(inputnode, flirt_followupto_0m, [('target_0m', 'reference'), 
                                                     ('target_followup', 'in_file')]),
                       (inputnode, fnirt_followupto_0m, [('target_0m', 'ref_file'), 
                                                         ('target_followup', 'in_file')]),
                       (flirt_followupto_0m, fnirt_followupto_0m, [('out_matrix_file','affine_file')]),
                       (inputnode, t1w_applyw, [('target_0m', 'ref_file'),
                                                ('t1w', 'in_file')]),
                       (fnirt_followupto_0m, t1w_applyw, [('field_file', 'field_file')]),
                       (inputnode, flair_applyw, [('target_0m', 'ref_file'),
                                                  ('flair', 'in_file')]),
                       (fnirt_followupto_0m, flair_applyw, [('field_file','field_file' )]),
                       (inputnode, mwf_applyw, [('target_0m', 'ref_file'),
                                                ('mwf', 'in_file')]),
                       (fnirt_followupto_0m, mwf_applyw, [('field_file', 'field_file')]),
                       (t1w_applyw, outputnode, [('out_file', 't1w_out_file')]),
                       (flair_applyw, outputnode, [('out_file', 'flair_out_file')]),
                       (mwf_applyw, outputnode, [('out_file', 'mwf_out_file')])
                      ]) 
    return reg_longi

def ants_reg_longi_wf():
    """ ants_reg_longi_wf 
        register sessions lonitudinal using ANTs
        use !brainextracted images! for target registration 
        t1w, flair and mwf images are warped to baseline target image ()

    """
    ##NODES
    #inputnode 
    inputnode= Node(IdentityInterface( fields=["flair","t1w", "mwf", "target_0m", "target_followup"]), name="inputnode")
    #target registration session12m to 0m
    baseline_coreg=Node(RegistrationSynQuick(transform_type="b"), name='baseline_coreg')
    merge= Node(Merge(2), name= "merge") 
    #ApplyWarp
    t1w_applyw= Node(ApplyTransforms(), name= 't1w_applyw')
    flair_applyw= Node(ApplyTransforms(), name= 'flair_applyw')
    mwf_applyw= Node(ApplyTransforms(), name= 'mwf_applyw')
    #outputnode
    outputnode=Node(IdentityInterface(fields=["flair_out_file", "t1w_out_file", "mwf_out_file", "spgr_coreg", 'affine_matrix', 'forward_warp_field']), name="outputnode")

    ##WORKFLOWS
    ants_reg_longi = Workflow(name="ants_reg_longi")
    ants_reg_longi.connect([(inputnode, baseline_coreg, [('target_0m', "fixed_image"), 
                                                    ('target_followup', "moving_image")]),
                       (baseline_coreg, merge, [("forward_warp_field", 'in1'),
                                                ("out_matrix", 'in2')]),
                       (inputnode, flair_applyw, [('target_0m', 'reference_image'),
                                                  ('flair', 'input_image' )]),
                       (merge, flair_applyw, [('out', 'transforms')]),


                       (inputnode, t1w_applyw, [('target_0m', 'reference_image'),
                                                ('t1w', 'input_image')]),
                       (merge, t1w_applyw, [('out', 'transforms')]),

                       (inputnode, mwf_applyw, [('target_0m', 'reference_image'),
                                                ('mwf', 'input_image')]),
                       (merge, mwf_applyw, [('out', 'transforms')]),

                       (t1w_applyw, outputnode, [('output_image', 't1w_out_file')]),
                       (flair_applyw, outputnode, [('output_image', 'flair_out_file')]),
                       (mwf_applyw, outputnode, [('output_image', 'mwf_out_file')]),
                       (baseline_coreg, outputnode, [('warped_image', 'spgr_coreg'),
                                                    ('out_matrix', 'affine_matrix'),
                                                    ('forward_warp_field', 'forward_warp_field')])
                      ]) 
    return ants_reg_longi
