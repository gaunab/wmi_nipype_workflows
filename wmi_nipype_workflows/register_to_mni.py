#! env python3

# -*- coding: utf-8 -*-

__author__ = "Paul Kuntke"
__email__ = "paul.kuntke @ukdd.de"
__license__ = "BSD-3-Clause"

from nipype.interfaces.fsl import Info
from nipype.interfaces.utility import Merge
from nipype import Node, MapNode, IdentityInterface, Workflow
from nipype.interfaces.ants import ApplyTransforms, RegistrationSynQuick


def coregister_to_mni_wf(skullstripped=False):
    """
    Register a T1w image to mni-Space, additionally add a list of Files to coregister to mni as well. By
    setting skullstripped to True an brain-extracted T1w  can be used as input. If `skullstripped` is False
    (Default) a full T1w is used.

    Parameters:
    -----------
    skullstripped: bool
        set to True if T1w is brain exctracted (default: False)

    Inputnode:
    ----------
    T1: T1w image to register to MNI-Space

    """

    MNI_template = Info.standard_image('MNI152_T1_1mm.nii.gz')
    if skullstripped:
        MNI_template = Info.standard_image('MNI152_T1_1mm_brain.nii.gz')

    inputnode = Node(IdentityInterface(fields=['t1', 'coreg_files']), name = 'inputnode')
    outputnode = Node(IdentityInterface(fields=['t1_registered', 'warp_field', 'affine_matrix', 'coregistered']), name = 'outputnode')
    warpnode = Node(RegistrationSynQuick(),  name='warpnode')
    warpnode.inputs.fixed_image = MNI_template

    transforms = Node(Merge(2), name='transforms') # Merge transforms from warpnode to single list
    apply_transforms = MapNode(ApplyTransforms(), name = 'apply_transforms', iterfield = ['input_image'])
    apply_transforms.inputs.reference_image = MNI_template

    wf = Workflow(name='coregister_to_mni')
    wf.connect([(inputnode, warpnode, [('t1', 'moving_image')]),
                (warpnode, transforms, [('forward_warp_field', 'in1'),
                                        ('out_matrix', 'in2')]),
                (transforms, apply_transforms, [('out', 'transforms')]),
                (inputnode, apply_transforms, [('coreg_files', 'input_image')]),
                (warpnode, outputnode, [('warped_image', 't1_registered'),
                                        ('forward_warp_field', 'warp_field'),
                                        ('out_matrix', 'affine_matrix')]),
                (apply_transforms, outputnode, [('output_image', 'coregistered')]),
               ])

    return wf



