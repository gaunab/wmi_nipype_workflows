#! env python

# ~*~ coding: utf-8 ~*~

import matplotlib
matplotlib.use('Agg')

import json
import nibabel as nb
from io import StringIO
import io
from nipype.utils import filemanip
from matplotlib.pyplot import figure
from nilearn import image as nlimage
from nilearn.plotting import plot_anat, plot_roi
from nipype.interfaces.mixins import reporting
from nipype.interfaces.base.core import BaseInterface, BaseInterfaceInputSpec
from nipype.interfaces.base import File, traits

from string import Template
from os import makedirs, listdir
from os.path import join, isdir, isfile, dirname, abspath

from shutil import copy2 as copy



def get_3d_file(in_file):
    """ if in_file is 3d, return it.
    if 4d, pick an arbitrary volume and return that.

    if in_file is a list of files, return an arbitrary file from
    the list, and an arbitrary volume from that file

    original source: niworkflows/viz/utils.py
    """

    in_file = filemanip.filename_to_list(in_file)[0]

    try:
        in_file = nb.load(in_file)
    except AttributeError:
        in_file = in_file

    if in_file.get_data().ndim == 3:
        return in_file

    return nlimage.index_img(in_file, 0)


def plot_segmentation(image, segmentation=None, outfile=None):
    """ Plot the segmentation of an image 

    Parameters
    ------------
    image: str
        Filename of Anatomical image 
    segmantation: str or list of str
        Filename(s) of segmented data
    outfile: str
        filename to save resulting svg (default='plot_anat.svg')

    """

    outfile = abspath(outfile or 'plot_anat.svg')
    if segmentation is None:
        segmentation = []
    segmentation = segmentation if type(segmentation) is list else [segmentation]

    image = get_3d_file(image)
    data = image.get_data()
    display = plot_anat(image)
    ## import ipdb
    ## ipdb.set_trace()
    plot_params= {}
    plot_params['linewidths'] = 0.5
    for seg in segmentation:
        seg_nib = get_3d_file(seg)
        display.add_contours(seg_nib, **plot_params)

    image_buf = StringIO()
    display.frame_axes.figure.savefig(image_buf, format='png', dpi=300)

    xml_str = image_buf.getvalue()
    start_index = xml_str.find('<svg ')
    end_index = xml_str.find('</svg>')

    svg_image = xml_str[start_index:end_index + len('<\svg>')]

    with open(outfile,'w') as fd:
        fd.write(svg_image)

    return outfile





def plot_registration():

    pass


def plot_masks(image, masks=None, outfile=None):
    """ Plot the mask of an image 

    Parameters
    ------------
    image: str
        Filename of Anatomical image 
    masks: str or list of str
        Filename(s) of segmented data
    outfile: str
        filename to save resulting svg (default='plot_anat.svg')

    """

    from numpy import zeros
    import nibabel as nib

    outfile = abspath(outfile or 'plot_mask.png')
    # Assure that masks is a list
    if masks is None:
        masks = []
    masks = masks if type(masks) is list else [masks]

    image = get_3d_file(image)
    data = image.get_data()

    mdata = zeros(image.shape)
    # Add all masks, with 
    for mask in masks:
        mask_data = nib.load(mask).get_data().astype(int) + mdata.max()
        mdata = mdata + mask_data
    nib_mdata = nib.Nifti1Image(mdata, image.affine, header=image.header)
    fig = figure(figsize=(20,9))
    display = plot_roi(roi_img=nib_mdata,
                       threshold=0.02,
                       bg_img=image,
                       black_bg=True,
                       cmap='jet',
                       figure=fig,
                       dim='auto',
                       resampling_interpolation='nearest'
                      )
    display.savefig(outfile)
    return outfile


def gen_report_dir(report_dir):
    """ Update or create Report dir with neccessary files like index.html, js-
    and css files.

    """

    assets_dir = join(report_dir, '_assets')
    assets_source = join(dirname(__file__), 'assets')

    # Make sure all neccessary files are inthe reports dir and copy
    # them as needed.
    for path in ["css", "js", "imgs", "fonts"]:
        dest_full_path = join(assets_dir, path)
        src_path = join(assets_source, '_assets', path)
        # Create dir
        if not isdir(dest_full_path):
            makedirs(dest_full_path)
        # Copy all files from source to dest if not extistent
        for f in listdir(src_path):
            if not isfile(join(dest_full_path, f)):
                copy(join(src_path, f), join(dest_full_path, f))

    if not isdir(join(report_dir, '_json')):
        makedirs(join(report_dir, '_json'))


def get_json_data_from_path(path_json):
    """Read all json files present in the given path, and output an aggregated json structure"""
    import glob

    results = []
    for file_json in glob.iglob(join(path_json, '*.json')):
        print(f"read file {file_json}")
        with open(file_json, 'r+') as fjson:
            results.append(json.load(fjson))
    return results

def update_html(report_dir):
    """ Update html-file with json data.

    Parameters:
    -----------
    report_dir: str
        Dirname of qc-output
    """
    json_data = get_json_data_from_path(join(report_dir,'_json'))
    assets_source = join(dirname(__file__), 'assets')

    with open(join(assets_source, 'index.html')) as template_idx:
        template = Template(template_idx.read())
        output = template.substitute(wmirpt_json_data=json.dumps(json_data))
        io.open(join(report_dir, 'index.html'), 'w').write(output)


class ReportingInterface(reporting.ReportCapableInterface):
    """Interface that always generates a report

    A subclass must define an ``input_spec`` and override ``_generate_report``.
    """
    output_spec = reporting.ReportCapableOutputSpec

    def __init__(self, generate_report=True, **kwargs):
        super(ReportingInterface, self).__init__(generate_report=generate_report, **kwargs)

    def _run_interface(self, runtime):
        return runtime

class SegmentationRPTInputSpecRPT(BaseInterfaceInputSpec):
    mask = File(exists=True, mandatory=True, desc="mask File or Files")
    in_file = File(exists=True, mandatory=True, desc='anatomical file, where mask should be projected on')
    report_dir = File('/tmp/wmi_workflow_report', usedefault=True, desc='directory where report is placed')
    subject_id = traits.Str('sub-0000', usedefault=True, desc='subject-ID')
    contrast = traits.Str('-', usedefault=True, desc='contrast description might be T1/T2...')
    command = traits.Str('-', usedefault=True, desc='Give function name')
    cmd = traits.Str('', usedefaults=True, desc='Command Line')

class SegmentationRPT(BaseInterface):
    """ Nipype Interface to create Reports for Segmentation-Results
    """
    input_spec=SegmentationRPTInputSpecRPT

    def _run_interface(self, runtime):
        import os
        from datetime import datetime
        from os.path import abspath, join
        from nilearn.plotting import plot_anat, show

        report_dir = self.inputs.report_dir
        subject_id = self.inputs.subject_id
        moddate = str(datetime.now())
        outdir_date = moddate.replace(' ', '_').replace(':', '')
        # create a dataset
        output_data = {
            "moddate": moddate,
            "subject": subject_id,
            "fname_in": str(self.inputs.in_file),
            "contrast": str(self.inputs.contrast),
            "command": str(self.inputs.command),
            "cmdline": str(self.inputs.cmd)
            }

        # Create the neccessary dirs
        target_img_folder = join(report_dir, 'data', subject_id, outdir_date)
        try:
            os.makedirs(target_img_folder)
        except OSError as err:
            if not os.path.isdir(target_img_folder):
                raise err


        # -- Generate Ouptut-Images --
        # first get a good position from roi/mask
        dimensions = nb.load(self.inputs.in_file).shape
        if len(dimensions) == 3:
            roi = plot_roi(self.inputs.mask, self.inputs.in_file)
            # display the image without roi 
            display = plot_anat(self.inputs.in_file,
                    draw_cross=False,
                    annotate=False,
                    cut_coords=roi.cut_coords)

        elif len(dimensions) == 4:
            in_img = nb.load(self.inputs.in_file)
            in_data = in_img.get_fdata()[:,:,:,0]
            in_mask = nb.load(self.inputs.mask)
            in_file_3d = nb.Nifti1Image(in_data, in_mask.affine, header=in_mask.header)
            roi = plot_roi(self.inputs.mask, in_file_3d)
            # display the image without roi 
            display = plot_anat(in_file_3d,
                    draw_cross=False,
                    annotate=False,
                    cut_coords=roi.cut_coords)

        # save the image
        display.savefig(join(target_img_folder, 'img.png'))
        # add the mask
        display.add_contours(self.inputs.mask, filled=True, colors='r')
        # save image with mask
        display.savefig(join(target_img_folder, 'img_overlay.png'))
        # store image-informations to report-dataset
        output_data['overlay_img'] = join('data', subject_id, outdir_date, 'img_overlay.png')
        output_data['background_img'] = join('data', subject_id, outdir_date, 'img.png')
        # (re-)create report files
        gen_report_dir(report_dir)

        # Write dataset to JSON-Report 
        with open(join(report_dir, '_json', f"qc_report_{outdir_date}.json"),'w', encoding='utf-8') as f:
            json.dump(output_data, f, ensure_ascii=False, indent=4)

        update_html(report_dir)

        return runtime

class RegistrationRPTInputSpecRPT(BaseInterfaceInputSpec):
    moving_img = File(exists=True, mandatory=True, desc="Registered to in_file")
    in_file = File(exists=True, mandatory=True, desc='anatomical file, where mask should be projected on')
    report_dir = File('/tmp/wmi_workflow_report', usedefault=True, desc='directory where report is placed')
    subject_id = traits.Str('sub-0000', usedefault=True, desc='subject-ID')
    contrast = traits.Str('-', usedefault=True, desc='contrast description might be T1/T2...')
    command = traits.Str('-', usedefault=True, desc='Give function name')
    cmd = traits.Str('', usedefaults=True, desc='Command Line')

class RegistrationRPT(BaseInterface):
    """ Nipype Interface to create Reports for Segmentation-Results
    """
    input_spec=RegistrationRPTInputSpecRPT

    def _run_interface(self, runtime):
        import os
        from datetime import datetime
        from os.path import abspath, join
        from nilearn.plotting import plot_anat, show

        report_dir = self.inputs.report_dir
        subject_id = self.inputs.subject_id
        moddate = str(datetime.now())
        outdir_date = moddate.replace(' ', '_').replace(':', '')
        # create a dataset
        output_data = {
            "moddate": moddate,
            "subject": subject_id,
            "fname_in": str(self.inputs.in_file),
            "contrast": str(self.inputs.contrast),
            "command": str(self.inputs.command),
            "cmdline": str(self.inputs.cmd)
            }

        # Create the neccessary dirs
        target_img_folder = join(report_dir, 'data', subject_id, outdir_date)
        try:
            os.makedirs(target_img_folder)
        except OSError as err:
            if not os.path.isdir(target_img_folder):
                raise err

        display = plot_anat(self.inputs.in_file,
                draw_cross=False,
                annotate=False)
        # save the image
        display.savefig(join(target_img_folder, 'img.png'))
        # add the mask
        display.add_contours(self.inputs.moving_img, filled=False, colors='g')
        # save image with mask
        display.savefig(join(target_img_folder, 'img_overlay.png'))
        # store image-informations to report-dataset
        output_data['overlay_img'] = join('data', subject_id, outdir_date, 'img_overlay.png')
        output_data['background_img'] = join('data', subject_id, outdir_date, 'img.png')
        # (re-)create report files
        gen_report_dir(report_dir)

        # Write dataset to JSON-Report 
        with open(join(report_dir, '_json', f"qc_report_{outdir_date}.json"),'w', encoding='utf-8') as f:
            json.dump(output_data, f, ensure_ascii=False, indent=4)

        update_html(report_dir)

        return runtime
