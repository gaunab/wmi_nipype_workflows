#! python
#
# ~*~ coding: utf-8 ~*~

__author__ = "Paul Kuntke"
__email__ = "paul.kuntke@ukdd.de"
__license__ = "BSD-3-Clause"

"""
Helper functions and Nipype Nodes

"""
def region_mean(fname, mask_file=None, label=None):
    """ Calculate the mean of all voxels. If a mask is given the Mean is only
    for the masked voxels. Optionally a label (or list of labels) might be
    given to select rois. Please make sure that the inputfile and mask have
    same alignment.

    If no mask is applied to this function all voxels != 0 are incorporated into mean calculation.
    (all voxels unequal to 0 will be used as mask)

    Returns:
    --------
    * float: mean of all voxels in given area
    or
    * list of floats: if a list of labels is given

    """
    import numpy as np
    import nibabel as nib
    from collections import Iterable

    img = nib.load(fname).get_data()
    mask = np.zeros(img.shape)

    if mask_file is None:
        # No mask used:
        mask[img != 0] = 1
    else:
        mask = nib.load(mask_file).get_data()

    # now check if mask and image sizes match
    if len(set([mask.shape, img.shape])) > 1:
        raise ValueError("Mask and image do not match")

    if label is None:
        # binarize mask if no label is given
        mask[mask != 0] = 1
        label = [1]

    if isinstance(label, (int, float)):
        # make a list if it is not yet
        label = [label]
    elif not isinstance(label, Iterable):
        raise TypeError("Label must be either number or list of numbers")

    means = []
    for lbl in label:
        means.append(img[mask == lbl].mean())

    # If there's only a single mean value - return the float value
    if len(means) == 1:
        return means[0]

    # Return the list of all means
    return means

def mean_rois():
    """ Create a nipype_Function interface to extract mean-values from a nifti-file

    Interface-Inputs:
        fname: str
            Filename to analyze
        mask: str
            Filename for mask (optional)
        label: int/float or list of int/float
            label where mean should be analyzed from

    Example
    -------
    Extract subject_id from filenames

    >>> label_extractor = Node(extract_labels(), name='label_extractor')
    >>> label_extractor.inputs.label = 'sub'

    Returns:
    --------
    Nipype-Function-Interface
    """

    from nipype.interfaces.utility import Function

    fn = Function(function=region_mean, input_names=['fname', 'mask_file', 'label'], output_names=['mean'])
    return fn




def bids_labels(fname, label=None, prefix=True):
    """ Extract labels from a given filename. If a label is given, the single
    value matching this label is returned. If label is None (default) a dict of
    all value-label-pairs is returned. If prefix is True, the returned value is
    prefixed with label-name.

    >>> bids_labels('sub-p001_ses-2m_space-patient_T1w.nii.gz')
    >>>    {'ses': '2m', 'space': 'patient', 'sub': 'p001', 'modality': 'T1w'}

    >>> bids_labels('sub-p001_ses-2m_space-patient_T1w.nii.gz', 'sub')
    >>>     'p001'

    Parameters:
    -----------
    fname: str
        Filename full path or basename
    label: str
        Label of interest
    prefix: bool
        Put Labelname in front of return

    Returns:
    --------

    dict: {label: value}

    """
    from os.path import basename, extsep

    if type(fname) is list:
        if len(fname) == 1:
            fname = fname[0]

    fname_base = basename(fname)

    # Find all key-value-pairs
    kv_pairs = fname_base.split('_')
    # Find all labels
    labels = {kv.split('-')[0]:  kv.split('-')[-1] for kv in kv_pairs if len(kv.split('-')) == 2}
    # Modality has no modality-label, so take the last entry right before file-extension
    labels['modality'] = kv_pairs[-1].split(extsep)[0]

    if not label is None:
        if prefix:
            labels[label] = label + "-" + labels[label]
        return labels[label]

    return labels


def extract_labels():
    """ Create a nipype_Function interface to extract values from a bids-filename 

    Interface-Inputs:
        fname: str
            Filename to analyze
        label: str
            Label to analyze

    Example
    -------
    Extract subject_id from filenames

    >>> label_extractor = Node(extract_labels(), name='label_extractor')
    >>> label_extractor.inputs.label = 'sub'

    Returns:
    --------
    Nipype-Function-Interface
    """

    from nipype.interfaces.utility import Function

    fn = Function(function=bids_labels, input_names=['fname', 'label'], output_names=['values'])
    return fn
